# Caelum Tokens


## Getting Started 
### Requirements 
Make sure you have installed `nodejs` and `npm`.

### Installation
Install all packages:
```sh
npm install 
```

## Available Tokens
### BasicToken
ERC777 token implementing `ERC777`.

### MintableToken
ERC777 token implementing `ERC777` and `ERC777Mintable`.
Where `ERC777Mintable` is an `ERC777` and a `MinterRole`.

### TTT Token
TODO

### USDA Token
TODO (Token representing 1 dollar).

### LIC Token (TTT Vault)
TODO (will use [TokenVesting](https://github.com/OpenZeppelin/openzeppelin-solidity/blob/master/contracts/drafts/TokenVesting.sol))