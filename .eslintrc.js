module.exports = {
    'env': {
        'browser': true,
        'commonjs': true,
        'es6': true,
        'mocha': true
    },
    'extends': 'standard',
    'globals': {
        'artifacts': false,
        'Atomics': 'readonly',
        'contract': false,
        'SharedArrayBuffer': 'readonly',
        'web3': false
    },
    'parserOptions': {
        'ecmaVersion': 2018
    },
    'rules': {
    }
}
