// Based on https://github.com/OpenZeppelin/openzeppelin-solidity/blob/master/test/examples/SimpleToken.test.js
const { BN, constants, expectEvent, singletons } = require('openzeppelin-test-helpers')
const { ZERO_ADDRESS } = constants

const { expect } = require('chai')

const BasicToken = artifacts.require('BasicToken')

contract('BasicToken', function ([_, creator]) {
  const _name = 'BasicToken'
  const _symbol = 'BCT'
  const _defaultOperators = []
  const _initialSupply = new BN(100)
  beforeEach(async function () {
    await singletons.ERC1820Registry(creator)
    this.token = await BasicToken.new(_name, _symbol, _initialSupply, _defaultOperators, { from: creator })
  })
  it('has a name', async function () {
    expect(await this.token.name()).to.equal(_name)
  })
  it('has a symbol', async function () {
    expect(await this.token.symbol()).to.equal(_symbol)
  })
  it('has 18 decimals', async function () {
    expect(await this.token.decimals()).to.be.bignumber.equal('18')
  })
  it('assigns the initial total supply to the creator', async function () {
    const totalSupply = await this.token.totalSupply()
    const creatorBalance = await this.token.balanceOf(creator)
    expect(creatorBalance).to.be.bignumber.equal(totalSupply)

    await expectEvent.inConstruction(this.token, 'Transfer', {
      from: ZERO_ADDRESS,
      to: creator,
      value: totalSupply
    })
  })
})
