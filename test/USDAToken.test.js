const { shouldBehaveLikeERC777Mintable } = require('./utils/behaviors/ERC777Mintable.behavior.js')

const { BN, constants, expectEvent, singletons } = require('openzeppelin-test-helpers')
const { ZERO_ADDRESS } = constants

const { expect } = require('chai')

const USDAToken = artifacts.require('USDAToken')

contract('USDAToken', function (accounts) {
  const _name = 'USDAToken'
  const _symbol = 'USDA'
  const _defaultOperators = []
  const _initialSupply = new BN(100)
  beforeEach(async function () {
    await singletons.ERC1820Registry(accounts[0])
    this.token = await USDAToken.new(_name, _symbol, _initialSupply, _defaultOperators, { from: accounts[0] })
  })
  it('has a name', async function () {
    expect(await this.token.name()).to.equal(_name)
  })
  it('has a symbol', async function () {
    expect(await this.token.symbol()).to.equal(_symbol)
  })
  it('has 18 decimals', async function () {
    expect(await this.token.decimals()).to.be.bignumber.equal('18')
  })
  it('assigns the initial total supply to the creator', async function () {
    const totalSupply = await this.token.totalSupply()
    const creatorBalance = await this.token.balanceOf(accounts[0])
    expect(creatorBalance).to.be.bignumber.equal(totalSupply)

    await expectEvent.inConstruction(this.token, 'Transfer', {
      from: ZERO_ADDRESS,
      to: accounts[0],
      value: totalSupply
    })
  })
  describe('minter role', function () {
    beforeEach(async function () {
      this.contract = this.token
      await this.contract.addMinter(accounts[1], { from: accounts[0] })
    })
  })
  shouldBehaveLikeERC777Mintable(accounts[0], accounts[2])
})
