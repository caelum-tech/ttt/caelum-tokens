pragma solidity ^0.5.4;

import "openzeppelin-solidity/contracts/token/ERC777/ERC777.sol";
import "openzeppelin-solidity/contracts/access/roles/MinterRole.sol";

contract ERC777Mintable is ERC777, MinterRole {
    /**
     * @dev See {ERC20-_mint}.
     *
     * Requirements:
     *
     * - the caller must have the {MinterRole}.
     */
    function mint(
        address operator,
        address account,
        uint256 amount,
        bytes memory userData,
        bytes memory operatorData
    ) public onlyMinter returns (bool) {
        _mint(
            operator,
            account,
            amount,
            userData,
            operatorData
        );
        return true;
    }
}
