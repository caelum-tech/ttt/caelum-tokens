pragma solidity ^0.5.4;

import "./ERC777Mintable.sol";
import "openzeppelin-solidity/contracts/token/ERC777/ERC777.sol";
import "openzeppelin-solidity/contracts/math/SafeMath.sol";
import "openzeppelin-solidity/contracts/utils/Address.sol";

contract MintableToken is ERC777, ERC777Mintable  {
    using SafeMath for uint256;
    using Address for address;

    constructor(
        string memory _name,
        string memory _symbol,
        uint256 _initial_supply,
        address[] memory _defaultOperators
    ) ERC777(_name, _symbol, _defaultOperators) public {
        require(_initial_supply > 0, "Amount has to be greater then 0");

        _mint(
            msg.sender,
            msg.sender,
            _initial_supply.mul(10 ** 18),
            new bytes(0),
            new bytes(0)
        );
    }
}
